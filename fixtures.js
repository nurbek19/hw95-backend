const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('users');
    } catch (e) {
        console.log('Collections were not present, skipping drop...');
    }

    await User.create({
        username: 'user',
        password: '123',
        role: 'user',
        email: 'youremail@gmail.com'
    }, {
        username: 'admin',
        password: '123',
        role: 'user',
        email: 'myemail@gmail.com'
    });

    db.close();
});