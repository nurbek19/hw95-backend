const express = require('express');
const router = express.Router();

const Event = require('../models/Event');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const createRouter = () => {

    router.get('/', auth, async (req, res) => {
        const friends = req.user.friends;

        if(friends.length > 0) {
            const events = [];
            let myEvents = [];

            await Event.find({user: req.user._id}).populate('user').then(result => {
                myEvents = result;
            });

            friends.forEach(friend => {
                events.push(Event.find({user: friend._id}));
            });

            Promise.all(events).then(results => {
                const allMessages = myEvents.concat(...results);

                res.send(allMessages.sort((a, b) => {
                    return new Date(a.date) - new Date(b.date);
                }).reverse());
            });

        } else {
            Event.find({user: req.user._id}).populate('user')
                .then(results => res.send(results.sort((a, b) => {
                    return new Date(a.date) - new Date(b.date);
                }).reverse()))
                .catch(() => res.sendStatus(500));
        }
    });

    router.post('/', [auth, permit('user')], (req, res) => {
        const event = req.body;
        event.user = req.user._id;

        const e = new Event(event);

        e.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    router.get('/:id', (req, res) => {
        Event.findOne({_id: req.params.id}).populate('user')
            .then(result => {
                if (result) res.send(result);
                else res.sendStatus(404);
            })
            .catch(() => res.sendStatus(500));
    });

    router.delete('/:id', [auth, permit('user')], (req, res) => {
        Event.deleteOne({_id: req.params.id})
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    return router;
};

module.exports = createRouter;