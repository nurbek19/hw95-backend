const express = require('express');
const nanoid = require('nanoid');
const request = require('request-promise-native');

const User = require('../models/User');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const config = require('../config');

const createRouter = () => {
    const router = express.Router();

    router.post('/', (req, res) => {
        const user = new User({
            username: req.body.username,
            password: req.body.password
        });

        user.save()
            .then(user => res.send(user))
            .catch(error => res.status(400).send(error));
    });


    router.post('/sessions', async (req, res) => {

        const user = await User.findOne({username: req.body.username});

        if (!user) {
            return res.status(400).send({error: 'Username not found'});
        }

        const isMatch = await user.checkPassword(req.body.password);

        if (!isMatch) {
            return res.status(400).send({error: 'Password is wrong!'});
        }

        const token = user.generateToken();

        return res.send({message: 'User and password correct', user, token});
    });

    router.put('/follow', auth, async (req, res) => {
        const user = await User.findOne({email: req.body.email});
        try {
            if (user) {
                const isFriend = await User.findOne({_id: req.user._id, friends: {$in: [user._id]}});
                if (isFriend) {
                    return res.send({message: 'You have already followed this user!'});
                }

                await User.findOneAndUpdate({_id: req.user._id}, {
                    $push: {
                        friends: {
                            _id: user._id,
                            username: user.username
                        }
                    }
                });
                return res.send({message: 'You successfully followed this user!'});
            }

        } catch (e) {
            res.status(400).send(e);
        }
    });

    router.get('/followers', auth, async (req, res) => {
        User.findOne({_id: req.user._id}).then(result => {
            if (result) res.send(result.friends);
            else res.sendStatus(404);
        })
            .catch(() => res.sendStatus(500));
    });

    router.delete('/followers/:id', [auth, permit('user')], async (req, res) => {
        const user = await User.findOne({_id: req.user._id});

        const friends = user.friends;
        const index = friends.findIndex(
            friend => friend.id === req.params.id,
        );

        user.friends.splice(index, 1);

        await user.save();

        return res.send({message: 'Follower deleted!'});
    });


    router.post('/verify', auth, (req, res) => {
        res.send({message: 'Token valid'});
    });

    router.post('/facebookLogin', async (req, res) => {
        const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${req.body.accessToken}&access_token=${config.facebook.appId}|${config.facebook.appSecret}`;

        try {
            const response = await request(debugTokenUrl);

            const decodedResponse = JSON.parse(response);

            if (decodedResponse.data.error) {
                return res.status(401).send({message: 'Facebook token incorrect'});
            }

            if (req.body.id !== decodedResponse.data.user_id) {
                return res.status(401).send({message: 'Wrong user ID'});
            }

            let user = await User.findOne({facebookId: req.body.id});

            if (!user) {
                user = new User({
                    username: req.body.email,
                    password: nanoid(),
                    facebookId: req.body.id,
                    email: req.body.email,
                });

                await user.save();
            }

            let token = user.generateToken();

            return res.send({message: 'Login or register successful', user, token});

        } catch (error) {
            return res.status(401).send({message: 'Facebook token incorrect'});
        }
    });

    router.delete('/sessions', async (req, res) => {
        const token = req.get('Token');
        const success = {message: 'Success'};

        if (!token) return res.send(success);

        const user = await User.findOne({token});

        if (!user) return res.send(success);

        user.generateToken();

        await user.save();

        return res.send(success);
    });

    return router;
};

module.exports = createRouter;
